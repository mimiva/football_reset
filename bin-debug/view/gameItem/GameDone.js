var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 游戏完成
 */
var GameDone = (function (_super) {
    __extends(GameDone, _super);
    function GameDone() {
        var _this = _super.call(this) || this;
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    GameDone.prototype.init = function () {
        var bg_png = Main.createBitmapByName("over_png");
        this.addChild(bg_png);
        Main.anchorCenter(bg_png);
        bg_png.x = this.stage.stageWidth / 2;
        bg_png.y = this.stage.stageHeight / 2;
        this.addScoreText();
        this.addBtn();
    };
    GameDone.prototype.addScoreText = function () {
        var text = new eui.Label();
        this.addChild(text);
        Main.dragGetPatn(text, this);
        text.size = 50;
        text.text = "" + ParamPool.TOTAL_SCORE;
        text.x = 590;
        text.y = 305;
        text.textColor = 0x4E43B5;
        text.strokeColor = 0xffffff;
        text.stroke = 2;
    };
    // 添加按钮
    GameDone.prototype.addBtn = function () {
        var _this = this;
        var miva = this;
        var btn_png = Main.createBitmapByName("ok_btn_png");
        this.addChild(btn_png);
        btn_png.x = 450;
        btn_png.y = 450;
        btn_png.touchEnabled = true;
        btn_png.once(egret.TouchEvent.TOUCH_TAP, function () {
            var tk_png = Main.createBitmapByName("post_png");
            miva.stage.addChild(tk_png);
            Main.anchorCenter(tk_png);
            tk_png.x = miva.stage.stageWidth / 2;
            tk_png.y = miva.stage.stageHeight / 2;
            console.log("成绩提交中");
            console.log("确认游戏结束");
            if (ParamPool.TOTAL_SCORE > ParamPool.BEST_SCORE)
                return _this.postScore();
            if (ParamPool.TOTAL_SCORE >= ParamPool.BEST_SCORE)
                return _this.postScore();
            window.location.href = window.location.href + "?" + Math.random();
        }, this);
    };
    // 提交成绩
    GameDone.prototype.postScore = function () {
        var miva = this;
        // 整理参数
        var score = ParamPool.TOTAL_SCORE;
        var nick_name = document.querySelector(".nick-name").innerHTML;
        var head_img = document.querySelector(".head-img").innerHTML;
        var openid = document.querySelector(".openid").innerHTML;
        // nick_name = nick_name.substring(1);
        var param = "nick_name=" + nick_name + "&head_img=" + head_img + "&score=" + score + "&openid=" + openid;
        // console.log(param)
        // alert(param)
        // var xhr = new XMLHttpRequest();
        // var stringData = JSON.stringify({
        //     nick_name,
        //     head_img,
        //     score,
        //     openid
        // });
        //  alert(stringData)
        // //POST请求
        // xhr.open('POST', ParamPool.BASE_URL + "/paihang");
        // xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // xhr.send(stringData)
        // xhr.onreadystatechange = function (res) {
        //     if (xhr.readyState == 4) {
        //         if (xhr.status == 200) {
        //             console.log(JSON.parse(xhr.responseText))
        //             alert(xhr.responseText)
        //         }
        //     }
        // }
        var request = new egret.HttpRequest();
        request.responseType = egret.HttpResponseType.TEXT;
        request.open(ParamPool.BASE_URL + "/paihang", egret.HttpMethod.POST);
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.send(param);
        request.addEventListener(egret.Event.COMPLETE, onGetComplete, this);
        request.addEventListener(egret.IOErrorEvent.IO_ERROR, onPostIOError, this);
        function onGetComplete(evt) {
            console.log(evt.currentTarget.response);
            // alert(evt.currentTarget.response)
            // alert(JSON.stringify(evt.currentTarget.response))
            window.location.href = ParamPool.PRO_PATH + "?" + Math.random();
        }
        function onPostIOError(evt) {
            // alert(evt.currentTarget.response)
            // alert(JSON.stringify(evt.currentTarget.response))
            var tk_png = Main.createBitmapByName("postErr_png");
            miva.addChild(tk_png);
            Main.anchorCenter(tk_png);
            tk_png.x = miva.stage.stageWidth / 2;
            tk_png.y = miva.stage.stageHeight / 2;
            egret.Tween.get(tk_png).wait(500).to({ alpha: 0 }, 200).call(function () {
                window.location.href = ParamPool.PRO_PATH + "?" + Math.random();
            });
            console.log("成绩提交失败!");
        }
    };
    return GameDone;
}(egret.Sprite));
__reflect(GameDone.prototype, "GameDone");

var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * bgm
 */
var MusicItem = (function (_super) {
    __extends(MusicItem, _super);
    function MusicItem() {
        var _this = _super.call(this) || this;
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    MusicItem.prototype.init = function () {
        this.startLoad();
    };
    MusicItem.prototype.startLoad = function () {
        this.onLoadComplete();
    };
    MusicItem.prototype.onLoadComplete = function () {
        //一个简单的播放按钮
        this.soundBtn = Main.createBitmapByName("bgm_open_png");
        //监听按钮的触摸事件
        this.soundBtn.touchEnabled = true;
        this.soundBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTouch, this);
        this.addChild(this.soundBtn);
    };
    MusicItem.prototype.onTouch = function () {
        if (ParamPool.SOUND_CHANNEL) {
            this.soundBtn.texture = RES.getRes("bgm_close_png");
            console.log(ParamPool.SOUND_CHANNEL);
            ParamPool.SOUND_CHANNEL.stop();
            ParamPool.SOUND_CHANNEL = null;
            return;
        }
        else {
            this.soundBtn.texture = RES.getRes("bgm_open_png");
        }
        ParamPool.SOUND_CHANNEL = ParamPool.BGM.play(0, -1);
    };
    return MusicItem;
}(egret.DisplayObjectContainer));
__reflect(MusicItem.prototype, "MusicItem");

var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 主角
 */
var RoleItem = (function (_super) {
    __extends(RoleItem, _super);
    function RoleItem() {
        var _this = _super.call(this) || this;
        _this.role_png = Main.createBitmapByName("role_1_png");
        _this.trueItem = new egret.Shape();
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    RoleItem.prototype.init = function () {
        var miva = this;
        miva.createRole();
        miva.createHitItem();
        miva.roleRun();
    };
    // 绘制角色
    RoleItem.prototype.createRole = function () {
        var miva = this;
        miva.addChild(miva.role_png);
        miva.anchorOffsetX = miva.width - 80;
        miva.anchorOffsetY = miva.height - 80;
    };
    // 绘制碰物体
    RoleItem.prototype.createHitItem = function () {
        var miva = this;
        miva.trueItem.graphics.beginFill(0, 0);
        miva.trueItem.graphics.drawRect(miva.width - 30, miva.height - 15, -50, -50);
        miva.trueItem.graphics.endFill();
        miva.addChild(miva.trueItem);
    };
    // 角色跑步
    RoleItem.prototype.roleRun = function () {
        var _this = this;
        var timer = new egret.Timer(1000 / 14);
        var num = 0;
        timer.addEventListener(egret.TimerEvent.TIMER, function () {
            if (!ParamPool.GAME_PAUSE)
                return;
            num++;
            if (num > 7)
                num = 1;
            _this.role_png.texture = RES.getRes("role_" + num + "_png");
        }, this);
        timer.start();
    };
    return RoleItem;
}(egret.Sprite));
__reflect(RoleItem.prototype, "RoleItem");

var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 球门
 */
var Goal = (function (_super) {
    __extends(Goal, _super);
    function Goal() {
        var _this = _super.call(this) || this;
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    Goal.prototype.init = function () {
        var miva = this;
        miva.drawDoor();
    };
    // 绘制门
    Goal.prototype.drawDoor = function () {
        var miva = this;
        var door = Main.createBitmapByName("door_png");
        miva.graphics.beginFill(0, 0);
        miva.graphics.drawRect(0, 0, 30, this.stage.stageHeight);
        miva.graphics.endFill();
        miva.addChild(door);
        Main.anchorCenter(door);
        door.anchorOffsetX = door.width;
        door.y = miva.height / 2 - 80;
        Main.anchorCenter(this);
    };
    return Goal;
}(egret.Sprite));
__reflect(Goal.prototype, "Goal");

var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 射门场景
 */
var Shoot = (function (_super) {
    __extends(Shoot, _super);
    function Shoot(question) {
        var _this = _super.call(this) || this;
        // 选项
        _this.num = 0;
        _this.trueQ = null;
        _this.question = question;
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    Shoot.prototype.init = function () {
        var miva = this;
        miva.drawPanel();
        miva.title();
        miva.options();
        miva.createMask();
    };
    // 创建遮罩
    Shoot.prototype.createMask = function () {
        var miva = this;
        Main.anchorCenter(miva);
        miva.x = miva.stage.stageWidth / 2;
        miva.y = miva.stage.stageHeight / 2;
        miva.graphics.beginFill(0, .3);
        miva.graphics.drawRect(-150, -150, miva.stage.stageWidth + 150, miva.stage.stageHeight + 150);
        miva.graphics.endFill();
        miva.touchEnabled = true;
        miva.addEventListener(egret.TouchEvent.TOUCH_BEGIN, Main.superStopPropagation, miva);
        miva.addEventListener(egret.TouchEvent.TOUCH_MOVE, Main.superStopPropagation, miva);
        miva.addEventListener(egret.TouchEvent.TOUCH_END, Main.superStopPropagation, miva);
    };
    //  绘制答题面板
    Shoot.prototype.drawPanel = function () {
        var miva = this;
        var panel = Main.createBitmapByName("cardPanel_png");
        var body = Main.createBitmapByName("dati_png");
        body.x = 160;
        body.y = 160;
        miva.addChild(panel);
        miva.addChild(body);
    };
    // 题目
    Shoot.prototype.title = function () {
        var miva = this;
        var title = new eui.Label();
        title.text = miva.question.Q;
        title.textColor = 0x3E5B84;
        title.x = 166;
        title.y = 128;
        title.width = 600;
        title.lineSpacing = 10;
        miva.addChild(title);
    };
    Shoot.prototype.options = function () {
        var _this = this;
        var miva = this;
        var options = miva.question.W;
        var timer = new egret.Timer(1000 / 30, 27);
        var role_png = Main.createBitmapByName("r_1_png");
        var role_name = "m";
        role_png.y = 40;
        miva.addChild(role_png);
        var _loop_1 = function (a) {
            var container = new egret.Sprite();
            container.graphics.beginFill(0, 0);
            container.graphics.drawRect(0, 0, 100, 100);
            container.graphics.endFill();
            var txt = new eui.Label();
            txt.text = " " + options[a];
            txt.width = 130;
            txt.textAlign = egret.HorizontalAlign.CENTER;
            txt.verticalAlign = egret.VerticalAlign.MIDDLE;
            txt.strokeColor = 0;
            txt.stroke = 2;
            container.addChild(txt);
            miva.addChild(container);
            container.x = 120 + (270) * miva.num;
            container.y = 240;
            miva.num++;
            // 在创建时收集正确答案
            if (a == miva.question.A) {
                miva.trueQ = txt;
            }
            container.touchEnabled = true;
            container.once(egret.TouchEvent.TOUCH_TAP, function () {
                if (a == miva.question.A) {
                    console.log("答对了");
                    ParamPool.TOTAL_SCORE += ParamPool.GET_SCORE;
                    miva.createTip("success_btn_png");
                }
                else {
                    console.log("答错了");
                    miva.createTip("error_btn_png");
                    egret.Tween.get(miva.trueQ, { loop: true }).call(function () {
                        miva.trueQ.textColor = 0xf45050;
                        miva.trueQ.strokeColor = 0xf45050;
                    }).wait(200).call(function () {
                        miva.trueQ.textColor = 0xffffff;
                        miva.trueQ.strokeColor = 0;
                    }).wait(200);
                }
                if (a == "A") {
                    role_name = "r";
                }
                else if (a == "C") {
                    role_name = "l";
                }
                timer.start();
            }, miva);
        };
        for (var a in options) {
            _loop_1(a);
        }
        var num = 0;
        timer.addEventListener(egret.TimerEvent.TIMER, function () {
            num++;
            role_png.texture = RES.getRes(role_name + "_" + num + "_png");
        }, this);
        timer.addEventListener(egret.TimerEvent.TIMER_COMPLETE, function () {
            if (ParamPool.QUESTION_NUM < 5) {
                ParamPool.GAME_PAUSE = true;
            }
            else {
                console.log("游戏完成");
                _this.stage.addChild(new GameDone());
                return;
            }
            miva.parent.removeChild(miva);
            num = 0;
        }, this);
    };
    // 创建提示
    Shoot.prototype.createTip = function (png_name) {
        var miva = this;
        var png = Main.createBitmapByName(png_name);
        miva.stage.addChild(png);
        Main.anchorCenter(png);
        png.x = this.stage.stageWidth / 2;
        png.y = this.stage.stageHeight / 2;
        egret.Tween.get(png).to({ y: png.y - 80, alpha: 0 }, 1000);
    };
    return Shoot;
}(egret.Sprite));
__reflect(Shoot.prototype, "Shoot");

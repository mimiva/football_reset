var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 对手
 */
var RivalItem = (function (_super) {
    __extends(RivalItem, _super);
    function RivalItem() {
        var _this = _super.call(this) || this;
        _this.otherRole_png = Main.createBitmapByName("rival_1_png");
        // 实际碰撞物体
        _this.item = new egret.Shape();
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    RivalItem.prototype.init = function () {
        var miva = this;
        miva.drawOtherRole();
        miva.hitItem();
        miva.run();
    };
    // 画人
    RivalItem.prototype.drawOtherRole = function () {
        var miva = this;
        var otherRole_png = miva.otherRole_png;
        miva.addChild(otherRole_png);
        miva.anchorOffsetX = 50;
        miva.anchorOffsetY = miva.height - 80;
    };
    // 绘制碰物体
    RivalItem.prototype.hitItem = function () {
        var miva = this;
        miva.item.graphics.beginFill(0, 0);
        miva.item.graphics.drawRect(30, miva.height - 20, 50, -50);
        miva.item.graphics.endFill();
        miva.addChild(miva.item);
    };
    RivalItem.prototype.run = function () {
        var _this = this;
        var timer = new egret.Timer(1000 / 14);
        var num = 0;
        timer.addEventListener(egret.TimerEvent.TIMER, function () {
            if (!ParamPool.GAME_PAUSE)
                return;
            num++;
            if (num > 4)
                num = 1;
            _this.otherRole_png.texture = RES.getRes("rival_" + num + "_png");
        }, this);
        timer.start();
    };
    return RivalItem;
}(egret.Sprite));
__reflect(RivalItem.prototype, "RivalItem");

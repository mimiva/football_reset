var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 地图
 */
var MapItem = (function (_super) {
    __extends(MapItem, _super);
    function MapItem() {
        var _this = _super.call(this) || this;
        // 记录三条线的位置
        _this.top = 350;
        _this.mid = 425;
        _this.btm = 510;
        // 绘制地图
        _this.bufferX = 0;
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    MapItem.prototype.init = function () {
        var miva = this;
        for (var a = 0; a < 7; a++)
            miva.drawMap();
    };
    MapItem.prototype.drawMap = function () {
        var miva = this;
        var bg_png = Main.createBitmapByName("bg_png");
        miva.addChild(bg_png);
        bg_png.x = miva.bufferX;
        miva.bufferX = bg_png.x + bg_png.width;
    };
    return MapItem;
}(egret.DisplayObjectContainer));
__reflect(MapItem.prototype, "MapItem");

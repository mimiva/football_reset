var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 场景主控
 */
var ScreenLayer = (function (_super) {
    __extends(ScreenLayer, _super);
    function ScreenLayer() {
        var _this = _super.call(this) || this;
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    ScreenLayer.prototype.init = function () {
        var miva = this;
        miva.createStartScreen();
        miva.getBestScore();
        ParamPool.BGM = RES.getRes("bgm_mp3");
        ParamPool.SET_INDEXED_DB_FUN();
    };
    // 创建开始界面
    ScreenLayer.prototype.createStartScreen = function () {
        var miva = this;
        var start = new Start();
        miva.addChild(start);
        Main.anchorCenter(start);
        start.x = this.stage.stageWidth / 2;
        start.y = this.stage.stageHeight / 2;
        start.addEventListener("start", function () {
            miva.removeChild(start);
            miva.createGameBody();
            ParamPool.SOUND_CHANNEL = ParamPool.BGM.play(0, -1);
        }, miva);
    };
    // 创建游戏内容
    ScreenLayer.prototype.createGameBody = function () {
        var miva = this;
        var game = new Game();
        miva.addChild(game);
    };
    // 获取用户最高分
    ScreenLayer.prototype.getBestScore = function () {
        var openid = document.querySelector(".openid").innerHTML;
        var request = new egret.HttpRequest();
        request.responseType = egret.HttpResponseType.TEXT;
        request.open(ParamPool.BASE_URL + "/bestScore", egret.HttpMethod.POST);
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.send("openid=" + openid);
        request.addEventListener(egret.Event.COMPLETE, function (evt) {
            console.log(evt.currentTarget.response);
            var res = JSON.parse(evt.currentTarget.response);
            if (res.length == 0)
                return;
            ParamPool.BEST_SCORE = res[0].score;
        }, this);
    };
    return ScreenLayer;
}(egret.DisplayObjectContainer));
__reflect(ScreenLayer.prototype, "ScreenLayer");

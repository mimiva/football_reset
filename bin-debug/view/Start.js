var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 开始场景
 */
var Start = (function (_super) {
    __extends(Start, _super);
    function Start() {
        var _this = _super.call(this) || this;
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    Start.prototype.init = function () {
        var miva = this;
        var bg_png = Main.createBitmapByName("index_png");
        var logo_left = Main.createBitmapByName("logo_left_png");
        var logo_right = Main.createBitmapByName("logo_right_png");
        miva.addChild(bg_png);
        miva.addChild(logo_left);
        miva.addChild(logo_right);
        logo_left.x = 140;
        logo_left.y = 55;
        logo_right.x = 975;
        logo_right.y = 55;
        // Main.dragGetPatn(logo_left, miva);
        // Main.dragGetPatn(logo_right, miva);
        miva.createStartBtn();
        miva.createNumText();
        miva.createPaihangBtn();
    };
    // 创建排行榜按钮
    Start.prototype.createPaihangBtn = function () {
        var miva = this;
        var btn_png = Main.createBitmapByName("paihang_btn_png");
        miva.addChild(btn_png);
        btn_png.x = 190;
        btn_png.y = 470;
        btn_png.touchEnabled = true;
        btn_png.addEventListener(egret.TouchEvent.TOUCH_TAP, function (evt) {
            var paihang = document.querySelector(".mimiva-box");
            paihang.style.display = "block";
        }, miva);
    };
    // 创建开始按钮
    Start.prototype.createStartBtn = function () {
        var miva = this;
        var btn_png = Main.createBitmapByName("start_btn_png");
        miva.addChild(btn_png);
        btn_png.x = 640;
        btn_png.y = 470;
        btn_png.touchEnabled = true;
        btn_png.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            console.log("游戏开始 !!");
            if (ParamPool.GAME_NUM <= 0)
                return miva.createNumTipAlert();
            ParamPool.GAME_NUM--;
            ParamPool.SET_GAME_NUM_FUN_IDB();
            miva.dispatchEvent(new egret.Event("start"));
        }, miva);
    };
    // 创建次数不足提示
    Start.prototype.createNumTipAlert = function () {
        var miva = this;
        var tk_png = Main.createBitmapByName("tk_png");
        miva.addChild(tk_png);
        Main.anchorCenter(tk_png);
        tk_png.x = miva.stage.stageWidth / 2;
        tk_png.y = miva.stage.stageHeight / 2;
        egret.Tween.get(tk_png).wait(500).to({ alpha: 0 }, 200);
        console.log("游戏次数不足 !!");
    };
    Start.prototype.createNumText = function () {
        var miva = this;
        var txt = Start.txt;
        Start.txt.text = " \u4ECA\u65E5\u5269\u4F59 " + ParamPool.GAME_NUM + " \u6B21";
        txt.textColor = 0xFEDB1D;
        txt.strokeColor = 0;
        txt.stroke = 2;
        txt.x = 480;
        txt.y = 455;
        this.addChild(txt);
    };
    // 创建次数文字提示
    Start.txt = new eui.Label();
    return Start;
}(egret.DisplayObjectContainer));
__reflect(Start.prototype, "Start");

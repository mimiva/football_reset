var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 游戏主体
 */
var Game = (function (_super) {
    __extends(Game, _super);
    function Game() {
        var _this = _super.call(this) || this;
        _this.mapItem = new MapItem();
        _this.roleItem = new RoleItem();
        // 添加成绩板
        _this.scorePanelNow = new eui.Label();
        // 添加历史成绩面板
        _this.scorePanelBest = new eui.Label();
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    Game.prototype.init = function () {
        var miva = this;
        // 初始化游戏
        ParamPool.QUESTION_BOX = RES.getRes("question_json");
        ParamPool.GAME_PAUSE = false;
        // 生成道具
        miva.createMapItem();
        miva.createRole();
        miva.controleRole();
        miva.questionCreate();
        miva.createBestScorePanel();
        // miva.createScorePanel();
        miva.createBgmControle();
        miva.createGameRule();
        for (var i = 1; i <= 5; i++)
            miva.createGoal(i);
        // 监听帧事件
        miva.addEventListener(egret.Event.ENTER_FRAME, miva.update, miva);
    };
    // 帧事件钩子
    Game.prototype.update = function () {
        if (!ParamPool.GAME_PAUSE)
            return;
        var miva = this;
        miva.mapItem.x -= ParamPool.GAME_SPEED;
        miva.scorePanelNow.text = "" + ParamPool.TOTAL_SCORE;
        // 收集移动距离
        var moveDistance = Math.abs(miva.mapItem.x);
        if (moveDistance % 500 == 0) {
            console.log("刷出一个敌人");
            miva.createRival();
        }
        miva.goalHit();
        miva.rivalHit();
    };
    // 背景音乐控制器
    Game.prototype.createBgmControle = function () {
        var miva = this;
        var musicItem = new MusicItem();
        miva.addChild(musicItem);
        musicItem.x = miva.stage.stageWidth - musicItem.width - 80;
        musicItem.y = 30;
    };
    // 创建地图
    Game.prototype.createMapItem = function () {
        var miva = this;
        miva.addChild(miva.mapItem);
    };
    // 创建游戏规则提示
    Game.prototype.createGameRule = function () {
        var miva = this;
        var container = new egret.Sprite();
        var rule_png = Main.createBitmapByName("rule_png");
        var close_btn = Main.createBitmapByName("ok_btn_png");
        Main.anchorCenter(rule_png);
        rule_png.x = miva.stage.stageWidth / 2;
        rule_png.y = miva.stage.stageHeight / 2;
        close_btn.x = 450;
        close_btn.y = 520;
        close_btn.touchEnabled = true;
        close_btn.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            miva.removeChild(container);
            miva.createCountDown();
        }, miva);
        miva.addChild(container);
        container.addChild(rule_png);
        container.addChild(close_btn);
        container.touchEnabled = true;
        container.addEventListener(egret.TouchEvent.TOUCH_BEGIN, Main.superStopPropagation, miva);
        container.addEventListener(egret.TouchEvent.TOUCH_MOVE, Main.superStopPropagation, miva);
        container.addEventListener(egret.TouchEvent.TOUCH_END, Main.superStopPropagation, miva);
    };
    // 创建游戏开始倒计时与提示
    Game.prototype.createCountDown = function () {
        var miva = this;
        var countDownTimer = new egret.Timer(1000, 3);
        var num_png = Main.createBitmapByName("num_3_png");
        var controleTip = Main.createBitmapByName("tip_png");
        var timer_num = 3;
        Main.anchorCenter(num_png);
        num_png.x = miva.stage.stageWidth / 2;
        num_png.y = miva.stage.stageHeight / 2 - 100;
        Main.anchorCenter(controleTip);
        controleTip.x = this.stage.stageWidth / 2;
        controleTip.y = this.stage.stageHeight / 2 + 100;
        miva.addChild(controleTip);
        miva.addChild(num_png);
        countDownTimer.addEventListener(egret.TimerEvent.TIMER, function () {
            timer_num--;
            num_png.texture = RES.getRes("num_" + timer_num + "_png");
        }, miva);
        countDownTimer.addEventListener(egret.TimerEvent.TIMER_COMPLETE, function () {
            miva.removeChild(num_png);
            miva.removeChild(controleTip);
            ParamPool.GAME_PAUSE = true;
        }, this);
        countDownTimer.start();
    };
    Game.prototype.createScorePanel = function () {
        var miva = this;
        var container = new egret.Sprite();
        var scorePanel_png = Main.createBitmapByName("timer_item_png");
        container.addChild(scorePanel_png);
        container.addChild(miva.scorePanelNow);
        miva.addChild(container);
        container.x = 80;
        container.y = 30;
        miva.scorePanelNow.x = 85;
        miva.scorePanelNow.y = 15;
        miva.scorePanelNow.text = "" + ParamPool.TOTAL_SCORE;
    };
    Game.prototype.createBestScorePanel = function () {
        var miva = this;
        var container = new egret.Sprite();
        var scorePanel_png = Main.createBitmapByName("histroy_png");
        container.addChild(scorePanel_png);
        container.addChild(miva.scorePanelBest);
        container.addChild(miva.scorePanelNow);
        miva.addChild(container);
        container.x = 80;
        container.y = 30;
        miva.scorePanelBest.x = 130;
        miva.scorePanelBest.y = 5;
        miva.scorePanelBest.text = "" + ParamPool.BEST_SCORE;
        miva.scorePanelNow.x = 330;
        miva.scorePanelNow.y = 5;
        miva.scorePanelNow.text = "" + ParamPool.TOTAL_SCORE;
    };
    // 添加主角到舞台中
    Game.prototype.createRole = function () {
        var miva = this;
        miva.addChild(miva.roleItem);
        miva.roleItem.x = 300;
        miva.roleItem.y = miva.mapItem.mid;
    };
    // 操作角色
    Game.prototype.controleRole = function () {
        var miva = this;
        // 初始触摸点
        var startPoint = new egret.Point();
        var comput = new egret.Point();
        miva.touchEnabled = true;
        miva.addEventListener(egret.TouchEvent.TOUCH_BEGIN, function (evt) {
            startPoint.x = evt.stageX;
            startPoint.y = evt.stageY;
            miva.stage.addEventListener(egret.TouchEvent.TOUCH_MOVE, touchMoveHandler, miva);
            miva.stage.once(egret.TouchEvent.TOUCH_END, touchEndHandler, miva);
        }, miva);
        function touchMoveHandler(evt) {
            comput.y = evt.stageY - startPoint.y;
        }
        function touchEndHandler(evt) {
            miva.stage.removeEventListener(egret.TouchEvent.TOUCH_MOVE, touchMoveHandler, miva);
            comput.y < 0 ? miva.roleTT() : miva.roleDD();
        }
    };
    // 角色上移操作
    Game.prototype.roleTT = function () {
        var miva = this;
        var move = null;
        if (miva.roleItem.y == miva.mapItem.mid)
            move = miva.mapItem.top;
        if (miva.roleItem.y == miva.mapItem.btm)
            move = miva.mapItem.mid;
        if (move)
            egret.Tween.get(miva.roleItem).to({ y: move }, 200);
        console.log("角色上移");
    };
    // 角色下移操作
    Game.prototype.roleDD = function () {
        var miva = this;
        var move = null;
        if (miva.roleItem.y == miva.mapItem.mid)
            move = miva.mapItem.btm;
        if (miva.roleItem.y == miva.mapItem.top)
            move = miva.mapItem.mid;
        if (move)
            egret.Tween.get(miva.roleItem).to({ y: move }, 200);
        console.log("角色下移");
    };
    // 添加对手到舞台中
    Game.prototype.createRival = function () {
        var miva = this;
        var rivalItem = new RivalItem();
        var random = Math.random() * 90;
        miva.addChild(rivalItem);
        rivalItem.x = miva.stage.stageWidth;
        if (random < 30) {
            rivalItem.y = miva.mapItem.top;
            miva.swapChildren(miva.roleItem, rivalItem);
        }
        else if (random > 60) {
            rivalItem.y = miva.mapItem.mid;
            miva.swapChildren(miva.roleItem, rivalItem);
        }
        else {
            rivalItem.y = miva.mapItem.btm;
        }
        ParamPool.RIVAL_POOL.push({ item: rivalItem, isLive: true });
        egret.Tween.get(rivalItem).to({ x: -100 }, 1000).call(function () {
            miva.removeChild(rivalItem);
            ParamPool.RIVAL_POOL.shift();
        });
    };
    // 题目初始化
    Game.prototype.questionCreate = function () {
        ParamPool.QUESTION_A = ParamPool.QUESTION_BOX["高新地产问题："];
        ParamPool.QUESTION_B = ParamPool.QUESTION_BOX["足球类问题："];
    };
    // 抽取题目方法
    Game.prototype.getQuestion = function () {
        var num = 0;
        var res = null;
        ParamPool.QUESTION_NUM <= 4 ? handler(ParamPool.QUESTION_A) : handler(ParamPool.QUESTION_B);
        function handler(item) {
            num = Math.floor(Math.random() * (item.length - 1));
            res = item[num];
            item.splice(num, 1);
            console.log(res, num, item);
        }
        return res;
    };
    // 把球门添加到舞台中
    Game.prototype.createGoal = function (num) {
        if (num === void 0) { num = 1; }
        var miva = this;
        var goal = new Goal();
        miva.mapItem.addChild(goal);
        ParamPool.GOAL_POOL.push({ item: goal, isLive: true });
        goal.x = num * 2600;
        goal.y = this.stage.stageHeight / 2;
    };
    // 球门碰撞检测
    Game.prototype.goalHit = function () {
        var miva = this;
        for (var i = 0; i < ParamPool.GOAL_POOL.length; i++) {
            var isGoalHit = ParamPool.GOAL_POOL[i].item.hitTestPoint(miva.roleItem.trueItem.x + 300, miva.roleItem.trueItem.y);
            if (isGoalHit && ParamPool.GOAL_POOL[i].isLive) {
                // 暂停跑动
                ParamPool.GAME_PAUSE = false;
                // 杀死这个球门
                ParamPool.GOAL_POOL[i].isLive = false;
                // 题号+1
                ParamPool.QUESTION_NUM++;
                // 弹出题目
                var shoot = new Shoot(miva.getQuestion());
                miva.addChild(shoot);
                console.log("开始答题");
            }
        }
    };
    // 对手碰撞检测
    Game.prototype.rivalHit = function () {
        var miva = this;
        for (var i = 0; i < ParamPool.RIVAL_POOL.length; i++) {
            var isHit = miva.roleItem.trueItem.hitTestPoint(ParamPool.RIVAL_POOL[i].item.x, ParamPool.RIVAL_POOL[i].item.y + 20);
            if (isHit && ParamPool.RIVAL_POOL[i].isLive) {
                // 杀死这个对手
                ParamPool.RIVAL_POOL[i].isLive = false;
                ParamPool.TOTAL_SCORE -= ParamPool.DEDUCTION;
                // 主角闪烁
                egret.Tween.get(miva.roleItem)
                    .to({ alpha: 0 })
                    .wait(100)
                    .to({ alpha: 1 })
                    .wait(100)
                    .to({ alpha: 0 })
                    .wait(100)
                    .to({ alpha: 1 })
                    .wait(100)
                    .to({ alpha: 0 })
                    .wait(100)
                    .to({ alpha: 1 });
                console.log("扣分");
            }
        }
    };
    return Game;
}(egret.DisplayObjectContainer));
__reflect(Game.prototype, "Game");

var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
/**
 * 变量池
 */
var ParamPool = (function () {
    function ParamPool() {
    }
    // 设置本地数据库
    ParamPool.SET_INDEXED_DB_FUN = function () {
        var indexedDB = window.indexedDB;
        if (!indexedDB) {
            alert('你的设备设置不支持IndexedDB');
            window.location.href = ParamPool.PRO_PATH + "?" + Math.random();
            return;
        }
        ParamPool.IDB = window.indexedDB.open("kujomiva_dsssssssss", 5);
        ParamPool.IDB.onsuccess = function (e) {
            console.log("本地数据库连接成功!");
            ParamPool.DB = e.target.result;
            console.log(ParamPool.DB);
            ParamPool.GET_GAME_NUM_FUN_IDB();
        };
        ParamPool.IDB.onerror = function (e) { console.log(e.currentTarget.error.message); };
        ParamPool.IDB.onupgradeneeded = function (e) {
            ParamPool.DB = e.target.result;
            var objStore = null;
            if (!ParamPool.DB.objectStoreNames.contains("football")) {
                objStore = ParamPool.DB.createObjectStore("football", { keyPath: "miva_id" });
            }
        };
    };
    // indexedDB初始化游戏次数方法
    ParamPool.ADD_GAME_NUM_FUN_IDB = function () {
        var req = ParamPool.DB.transaction(["football"], "readwrite")
            .objectStore("football")
            .add({ miva_id: 1, day: new Date().getDate(), num: ParamPool.GAME_NUM });
        req.onsuccess = function (evt) {
            console.log("游戏次数设置成功, 剩余:", ParamPool.GAME_NUM, "次");
        };
        req.onerror = function (evt) {
            console.log("游戏次数设置失败");
            alert("游戏次数设置失败(初始化)");
            window.location.href = ParamPool.PRO_PATH + "?" + Math.random();
        };
    };
    // indexedDB设置游戏次数方法
    ParamPool.SET_GAME_NUM_FUN_IDB = function () {
        var req = ParamPool.DB.transaction(["football"], "readwrite")
            .objectStore("football")
            .put({ miva_id: 1, day: new Date().getDate(), num: ParamPool.GAME_NUM });
        req.onsuccess = function (evt) {
            console.log("游戏次数设置成功, 剩余:", ParamPool.GAME_NUM, "次");
        };
        req.onerror = function (evt) {
            console.log("游戏次数设置失败");
            alert("游戏次数设置失败(更新)");
            window.location.href = ParamPool.PRO_PATH + "?" + Math.random();
        };
    };
    // 从indexedDb中读取游戏次数
    ParamPool.GET_GAME_NUM_FUN_IDB = function () {
        var transaction = ParamPool.DB.transaction(["football"]);
        var objectStore = transaction.objectStore('football');
        var req = objectStore.get(1);
        req.onerror = function (evt) {
            console.log("事务失败");
            window.location.href = ParamPool.PRO_PATH + "?" + Math.random();
        };
        req.onsuccess = function (evt) {
            if (req.result) {
                console.log(req.result);
                if (req.result.day != new Date().getDate())
                    return ParamPool.SET_GAME_NUM_FUN_IDB();
                ParamPool.GAME_NUM = req.result.num;
                Start.txt.text = " \u4ECA\u65E5\u5269\u4F59 " + ParamPool.GAME_NUM + " \u6B21";
            }
            else {
                console.log("未获得数据记录");
                ParamPool.ADD_GAME_NUM_FUN_IDB();
            }
        };
    };
    // 敌方英雄池
    ParamPool.RIVAL_POOL = [];
    // 碰撞减分
    ParamPool.DEDUCTION = 1;
    // 球门池
    ParamPool.GOAL_POOL = [];
    // 答题得分
    ParamPool.GET_SCORE = 10;
    // 考卷
    ParamPool.QUESTION_BOX = {};
    // A卷
    ParamPool.QUESTION_A = [];
    // B卷
    ParamPool.QUESTION_B = [];
    // 题号
    ParamPool.QUESTION_NUM = 0;
    // 总分数
    ParamPool.TOTAL_SCORE = 50;
    // 最佳成绩
    ParamPool.BEST_SCORE = 0;
    // 游戏暂停开关
    ParamPool.GAME_PAUSE = true;
    // 游戏速度
    ParamPool.GAME_SPEED = 10;
    // 游戏次数
    ParamPool.GAME_NUM = 5;
    // 游戏版本号
    ParamPool.GAME_VER = "0.0.4";
    // 数据接口地址
    ParamPool.BASE_URL = "http://59.110.214.170:23333";
    // static BASE_URL = "http://127.0.0.1:7001";
    // 项目地址
    ParamPool.PRO_PATH = "http://www.unmcc.com/unmcc/2019/03/football";
    // 本地数据库
    ParamPool.IDB = null;
    ParamPool.DB = null;
    // BGM
    ParamPool.BGM = null;
    ParamPool.SOUND_CHANNEL = null;
    return ParamPool;
}());
__reflect(ParamPool.prototype, "ParamPool");

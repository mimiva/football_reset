/**
 * 开始场景
 */
class Start extends egret.DisplayObjectContainer {
    constructor() {
        super();
        this.once(egret.Event.ADDED_TO_STAGE, this.init, this);
    }

    private init() {
        const miva = this;
        const bg_png = Main.createBitmapByName("index_png");
        const logo_left = Main.createBitmapByName("logo_left_png");
        const logo_right = Main.createBitmapByName("logo_right_png");

        miva.addChild(bg_png);
        miva.addChild(logo_left);
        miva.addChild(logo_right);

        logo_left.x = 140;
        logo_left.y = 55;
        logo_right.x = 975;
        logo_right.y = 55;

        // Main.dragGetPatn(logo_left, miva);
        // Main.dragGetPatn(logo_right, miva);

        miva.createStartBtn();
        miva.createNumText();
        miva.createPaihangBtn();
    }
    // 创建排行榜按钮
    createPaihangBtn() {
        const miva = this;
        const btn_png = Main.createBitmapByName("paihang_btn_png");

        miva.addChild(btn_png);

        btn_png.x = 190;
        btn_png.y = 470;

        btn_png.touchEnabled = true;
        btn_png.addEventListener(egret.TouchEvent.TOUCH_TAP, evt => {
            const paihang = document.querySelector(".mimiva-box") as HTMLElement;
            paihang.style.display = "block"
        }, miva);
    }
    // 创建开始按钮
    createStartBtn() {
        const miva = this;
        const btn_png = Main.createBitmapByName("start_btn_png");

        miva.addChild(btn_png);

        btn_png.x = 640;
        btn_png.y = 470;

        btn_png.touchEnabled = true;
        btn_png.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            console.log("游戏开始 !!");
            if (ParamPool.GAME_NUM <= 0) return miva.createNumTipAlert();
            ParamPool.GAME_NUM--;
            ParamPool.SET_GAME_NUM_FUN_IDB();
            miva.dispatchEvent(new egret.Event("start"));
        }, miva);
    }
    // 创建次数不足提示
    createNumTipAlert() {
        const miva = this;
        const tk_png = Main.createBitmapByName("tk_png");
        miva.addChild(tk_png);
        Main.anchorCenter(tk_png);
        tk_png.x = miva.stage.stageWidth / 2;
        tk_png.y = miva.stage.stageHeight / 2;
        egret.Tween.get(tk_png).wait(500).to({ alpha: 0 }, 200);
        console.log("游戏次数不足 !!")
    }
    // 创建次数文字提示
    static txt = new eui.Label();
    createNumText() {
        const miva = this;
        const txt = Start.txt;
        Start.txt.text = ` 今日剩余 ${ParamPool.GAME_NUM} 次`;
        txt.textColor = 0xFEDB1D;
        txt.strokeColor = 0;
        txt.stroke = 2;
        txt.x = 480;
        txt.y = 455;
        this.addChild(txt);
    }
}
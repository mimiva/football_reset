/**
 * 场景主控
 */
class ScreenLayer extends egret.DisplayObjectContainer {
    constructor() {
        super();
        this.once(egret.Event.ADDED_TO_STAGE, this.init, this);
    }

    private init() {
        const miva = this;

        miva.createStartScreen();
        miva.getBestScore();

        ParamPool.BGM = RES.getRes("bgm_mp3");
        ParamPool.SET_INDEXED_DB_FUN();
    }
    // 创建开始界面
    private createStartScreen() {
        const miva = this;
        const start = new Start();

        miva.addChild(start);
        Main.anchorCenter(start);
        start.x = this.stage.stageWidth / 2;
        start.y = this.stage.stageHeight / 2;

        start.addEventListener("start", () => {
            miva.removeChild(start);
            miva.createGameBody();
            ParamPool.SOUND_CHANNEL = ParamPool.BGM.play(0, -1);
        }, miva);
    }
    // 创建游戏内容
    private createGameBody() {
        const miva = this;
        const game = new Game();
        miva.addChild(game);
    }
    // 获取用户最高分
    private getBestScore() {
        const openid = document.querySelector(".openid").innerHTML;
        var request = new egret.HttpRequest();
        request.responseType = egret.HttpResponseType.TEXT;
        request.open(ParamPool.BASE_URL + "/bestScore", egret.HttpMethod.POST);
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.send(`openid=${openid}`);
        request.addEventListener(egret.Event.COMPLETE, evt => {
            console.log(evt.currentTarget.response)
            const res = JSON.parse(evt.currentTarget.response);
            if (res.length == 0) return;
            ParamPool.BEST_SCORE = res[0].score;
        }, this);
    }

}
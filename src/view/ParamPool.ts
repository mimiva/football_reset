/**
 * 变量池
 */
class ParamPool {
    // 敌方英雄池
    static RIVAL_POOL = [];
    // 碰撞减分
    static DEDUCTION = 1;
    // 球门池
    static GOAL_POOL = [];
    // 答题得分
    static GET_SCORE = 10;
    // 考卷
    static QUESTION_BOX = {};
    // A卷
    static QUESTION_A = [];
    // B卷
    static QUESTION_B = [];
    // 题号
    static QUESTION_NUM = 0;
    // 总分数
    static TOTAL_SCORE = 50;
    // 最佳成绩
    static BEST_SCORE = 0;
    // 游戏暂停开关
    static GAME_PAUSE = true;
    // 游戏速度
    static GAME_SPEED = 10;
    // 游戏次数
    static GAME_NUM = 5;
    // 游戏版本号
    static GAME_VER = "0.0.4";
    // 数据接口地址
    static BASE_URL = "http://59.110.214.170:23333";
    // static BASE_URL = "http://127.0.0.1:7001";
    // 项目地址
    static PRO_PATH = "http://www.unmcc.com/unmcc/2019/03/football";
    // 本地数据库
    static IDB = null;
    static DB = null;
    // BGM
    static BGM = null;
    static SOUND_CHANNEL = null;

    // 设置本地数据库
    static SET_INDEXED_DB_FUN() {
        const indexedDB = window.indexedDB;

        if (!indexedDB) {
            alert('你的设备设置不支持IndexedDB');
            window.location.href = ParamPool.PRO_PATH + "?" + Math.random();
            return;
        }

        ParamPool.IDB = window.indexedDB.open("kujomiva_dsssssssss", 5);

        ParamPool.IDB.onsuccess = e => {
            console.log("本地数据库连接成功!");
            ParamPool.DB = e.target.result;
            console.log(ParamPool.DB)
            ParamPool.GET_GAME_NUM_FUN_IDB();
        };
        ParamPool.IDB.onerror = e => { console.log(e.currentTarget.error.message) }

        ParamPool.IDB.onupgradeneeded = e => {
            ParamPool.DB = e.target.result;
            let objStore = null;
            if (!ParamPool.DB.objectStoreNames.contains("football")) {
                objStore = ParamPool.DB.createObjectStore("football", { keyPath: "miva_id" });
            }
        }
    }
    // indexedDB初始化游戏次数方法
    static ADD_GAME_NUM_FUN_IDB() {
        const req = ParamPool.DB.transaction(["football"], "readwrite")
            .objectStore("football")
            .add({ miva_id: 1, day: new Date().getDate(), num: ParamPool.GAME_NUM });

        req.onsuccess = evt => {
            console.log("游戏次数设置成功, 剩余:", ParamPool.GAME_NUM, "次");
        }

        req.onerror = evt => {
            console.log("游戏次数设置失败");
            alert("游戏次数设置失败(初始化)");
            window.location.href = ParamPool.PRO_PATH + "?" + Math.random();
        }
    }
    // indexedDB设置游戏次数方法
    static SET_GAME_NUM_FUN_IDB() {
        const req = ParamPool.DB.transaction(["football"], "readwrite")
            .objectStore("football")
            .put({ miva_id: 1, day: new Date().getDate(), num: ParamPool.GAME_NUM });

        req.onsuccess = evt => {
            console.log("游戏次数设置成功, 剩余:", ParamPool.GAME_NUM, "次");
        }

        req.onerror = evt => {
            console.log("游戏次数设置失败");
            alert("游戏次数设置失败(更新)");
            window.location.href = ParamPool.PRO_PATH + "?" + Math.random();
        }
    }
    // 从indexedDb中读取游戏次数
    static GET_GAME_NUM_FUN_IDB() {
        const transaction = ParamPool.DB.transaction(["football"]);
        const objectStore = transaction.objectStore('football');
        const req = objectStore.get(1);

        req.onerror = evt => {
            console.log("事务失败");
            window.location.href = ParamPool.PRO_PATH + "?" + Math.random();
        }

        req.onsuccess = evt => {
            if (req.result) {
                console.log(req.result);
                if (req.result.day != new Date().getDate()) return ParamPool.SET_GAME_NUM_FUN_IDB();
                ParamPool.GAME_NUM = req.result.num;
                Start.txt.text = ` 今日剩余 ${ParamPool.GAME_NUM} 次`;
            } else {
                console.log("未获得数据记录");
                ParamPool.ADD_GAME_NUM_FUN_IDB();
            }
        }
    }
}
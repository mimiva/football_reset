/**
 * 主角
 */
class RoleItem extends egret.Sprite {
    private role_png: egret.Bitmap = Main.createBitmapByName("role_1_png");
    public trueItem = new egret.Shape();

    constructor() {
        super();
        this.once(egret.Event.ADDED_TO_STAGE, this.init, this);
    }

    private init() {
        const miva = this;
        miva.createRole();
        miva.createHitItem();
        miva.roleRun();
    }
    // 绘制角色
    private createRole() {
        const miva = this;
        miva.addChild(miva.role_png);
        miva.anchorOffsetX = miva.width - 80;
        miva.anchorOffsetY = miva.height - 80;
    }
    // 绘制碰物体
    private createHitItem() {
        const miva = this;
        miva.trueItem.graphics.beginFill(0, 0);
        miva.trueItem.graphics.drawRect(miva.width - 30, miva.height - 15, -50, -50);
        miva.trueItem.graphics.endFill();
        miva.addChild(miva.trueItem);
    }
    // 角色跑步
    private roleRun() {
        const timer = new egret.Timer(1000 / 14);
        let num = 0;
        timer.addEventListener(egret.TimerEvent.TIMER, () => {
            if (!ParamPool.GAME_PAUSE) return;
            num++;
            if (num > 7) num = 1;
            this.role_png.texture = RES.getRes(`role_${num}_png`);
        }, this);

        timer.start();
    }
}
/**
 * 球门
 */
class Goal extends egret.Sprite {
    constructor() {
        super();
        this.once(egret.Event.ADDED_TO_STAGE, this.init, this);
    }

    private init() {
        const miva = this;
        miva.drawDoor();
    }
    // 绘制门
    private drawDoor() {
        const miva = this;
        const door = Main.createBitmapByName("door_png");

        miva.graphics.beginFill(0, 0);
        miva.graphics.drawRect(0, 0, 30, this.stage.stageHeight);
        miva.graphics.endFill();

        miva.addChild(door);
        Main.anchorCenter(door);
        door.anchorOffsetX = door.width;
        door.y = miva.height / 2 - 80;
        Main.anchorCenter(this);
    }
}
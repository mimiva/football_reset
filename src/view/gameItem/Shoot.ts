/**
 * 射门场景
 */
class Shoot extends egret.Sprite {
    private question;

    constructor(question) {
        super();
        this.question = question;
        this.once(egret.Event.ADDED_TO_STAGE, this.init, this);
    }

    private init() {
        const miva = this;
        miva.drawPanel();
        miva.title();
        miva.options();
        miva.createMask();
    }
    // 创建遮罩
    private createMask() {
        const miva = this;

        Main.anchorCenter(miva);
        miva.x = miva.stage.stageWidth / 2;
        miva.y = miva.stage.stageHeight / 2;

        miva.graphics.beginFill(0, .3);
        miva.graphics.drawRect(-150, -150, miva.stage.stageWidth + 150, miva.stage.stageHeight + 150);
        miva.graphics.endFill();

        miva.touchEnabled = true;
        miva.addEventListener(egret.TouchEvent.TOUCH_BEGIN, Main.superStopPropagation, miva);
        miva.addEventListener(egret.TouchEvent.TOUCH_MOVE, Main.superStopPropagation, miva);
        miva.addEventListener(egret.TouchEvent.TOUCH_END, Main.superStopPropagation, miva);
    }
    //  绘制答题面板
    private drawPanel() {
        const miva = this;
        const panel = Main.createBitmapByName("cardPanel_png");
        const body = Main.createBitmapByName("dati_png");

        body.x = 160;
        body.y = 160;

        miva.addChild(panel);
        miva.addChild(body);
    }
    // 题目
    private title() {
        const miva = this;
        const title = new eui.Label();
        title.text = miva.question.Q;
        title.textColor = 0x3E5B84;
        title.x = 166;
        title.y = 128;
        title.width = 600;
        title.lineSpacing = 10;

        miva.addChild(title);
    }
    // 选项
    private num = 0;
    private trueQ = null;
    private options() {
        const miva = this;
        const options = miva.question.W;
        const timer = new egret.Timer(1000 / 30, 27);
        const role_png = Main.createBitmapByName("r_1_png");
        let role_name = "m";

        role_png.y = 40;

        miva.addChild(role_png);

        for (let a in options) {
            const container = new egret.Sprite();

            container.graphics.beginFill(0, 0);
            container.graphics.drawRect(0, 0, 100, 100);
            container.graphics.endFill();

            const txt = new eui.Label();
            txt.text = ` ${options[a]}`;
            txt.width = 130;
            txt.textAlign = egret.HorizontalAlign.CENTER;
            txt.verticalAlign = egret.VerticalAlign.MIDDLE;
            txt.strokeColor = 0;
            txt.stroke = 2;
            container.addChild(txt);

            miva.addChild(container);

            container.x = 120 + (270) * miva.num;
            container.y = 240;
            miva.num++;

            // 在创建时收集正确答案
            if (a == miva.question.A) {
                miva.trueQ = txt;
            }

            container.touchEnabled = true;
            container.once(egret.TouchEvent.TOUCH_TAP, () => {

                if (a == miva.question.A) {
                    console.log("答对了")
                    ParamPool.TOTAL_SCORE += ParamPool.GET_SCORE;
                    miva.createTip("success_btn_png")
                } else {
                    console.log("答错了")
                    miva.createTip("error_btn_png");
                    egret.Tween.get(miva.trueQ, { loop: true }).call(() => {
                        miva.trueQ.textColor = 0xf45050;
                        miva.trueQ.strokeColor = 0xf45050;
                    }).wait(200).call(() => {
                        miva.trueQ.textColor = 0xffffff;
                        miva.trueQ.strokeColor = 0;
                    }).wait(200);
                }

                if (a == "A") {
                    role_name = "r"
                } else if (a == "C") {
                    role_name = "l"
                }

                timer.start();
            }, miva);

        }

        let num = 0;
        timer.addEventListener(egret.TimerEvent.TIMER, () => {
            num++;
            role_png.texture = RES.getRes(`${role_name}_${num}_png`);
        }, this);

        timer.addEventListener(egret.TimerEvent.TIMER_COMPLETE, () => {
            if (ParamPool.QUESTION_NUM < 5) {
                ParamPool.GAME_PAUSE = true;
            } else {
                console.log("游戏完成");
                this.stage.addChild(new GameDone());
                return;
            }
            miva.parent.removeChild(miva);

            num = 0;
        }, this);
    }

    // 创建提示
    private createTip(png_name) {
        const miva = this;
        const png = Main.createBitmapByName(png_name);
        miva.stage.addChild(png);
        Main.anchorCenter(png);
        png.x = this.stage.stageWidth / 2;
        png.y = this.stage.stageHeight / 2;
        egret.Tween.get(png).to({ y: png.y - 80, alpha: 0 }, 1000)
    }
}
/**
 * 地图
 */
class MapItem extends egret.DisplayObjectContainer {
    // 记录三条线的位置
    public top = 350;
    public mid = 425;
    public btm = 510;

    constructor() {
        super();
        this.once(egret.Event.ADDED_TO_STAGE, this.init, this);
    }

    private init() {
        const miva = this;

        for (let a = 0; a < 7; a++) miva.drawMap();
    }
    // 绘制地图
    private bufferX = 0;
    private drawMap() {
        const miva = this;

        const bg_png = Main.createBitmapByName("bg_png");
        miva.addChild(bg_png);

        bg_png.x = miva.bufferX;
        miva.bufferX = bg_png.x + bg_png.width;
    }
}
/** 
 * 对手
 */
class RivalItem extends egret.Sprite {
    private otherRole_png: egret.Bitmap = Main.createBitmapByName("rival_1_png");
    // 实际碰撞物体
    public item: egret.Shape = new egret.Shape();

    constructor() {
        super();
        this.once(egret.Event.ADDED_TO_STAGE, this.init, this);
    }

    private init() {
        const miva = this;
        miva.drawOtherRole();
        miva.hitItem();
        miva.run();
    }
    // 画人
    private drawOtherRole() {
        const miva = this;
        const otherRole_png: egret.Bitmap = miva.otherRole_png;
        miva.addChild(otherRole_png);
        miva.anchorOffsetX = 50;
        miva.anchorOffsetY = miva.height - 80;
    }
    // 绘制碰物体
    private hitItem() {
        const miva = this;
        miva.item.graphics.beginFill(0, 0);
        miva.item.graphics.drawRect(30, miva.height - 20, 50, -50);
        miva.item.graphics.endFill();
        miva.addChild(miva.item);
    }
    private run() {
        const timer = new egret.Timer(1000 / 14);
        let num = 0;
        timer.addEventListener(egret.TimerEvent.TIMER, () => {
            if (!ParamPool.GAME_PAUSE) return;
            num++;
            if (num > 4) num = 1;
            this.otherRole_png.texture = RES.getRes(`rival_${num}_png`);
        }, this);

        timer.start();
    }
}
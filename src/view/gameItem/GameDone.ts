/**
 * 游戏完成
 */
class GameDone extends egret.Sprite {
    constructor() {
        super();

        this.once(egret.Event.ADDED_TO_STAGE, this.init, this);
    }

    private init() {
        const bg_png = Main.createBitmapByName("over_png");
        this.addChild(bg_png)
        Main.anchorCenter(bg_png)
        bg_png.x = this.stage.stageWidth / 2;
        bg_png.y = this.stage.stageHeight / 2;

        this.addScoreText();
        this.addBtn()
    }
    addScoreText() {
        const text = new eui.Label();

        this.addChild(text);
        Main.dragGetPatn(text, this);
        text.size = 50;
        text.text = `${ParamPool.TOTAL_SCORE}`;
        text.x = 590;
        text.y = 305;
        text.textColor = 0x4E43B5;
        text.strokeColor = 0xffffff;
        text.stroke = 2;
    }
    // 添加按钮
    addBtn() {
        const miva = this;
        const btn_png = Main.createBitmapByName("ok_btn_png");

        this.addChild(btn_png);

        btn_png.x = 450;
        btn_png.y = 450;

        btn_png.touchEnabled = true;
        btn_png.once(egret.TouchEvent.TOUCH_TAP, () => {

            const tk_png = Main.createBitmapByName("post_png");
            miva.stage.addChild(tk_png);
            Main.anchorCenter(tk_png);
            tk_png.x = miva.stage.stageWidth / 2;
            tk_png.y = miva.stage.stageHeight / 2;
            console.log("成绩提交中")

            console.log("确认游戏结束");
            if (ParamPool.TOTAL_SCORE > ParamPool.BEST_SCORE) return this.postScore();
            if (ParamPool.TOTAL_SCORE >= ParamPool.BEST_SCORE) return this.postScore();
            window.location.href = window.location.href + "?" + Math.random();
        }, this);

    }

    // 提交成绩
    postScore() {
        const miva = this;


        // 整理参数
        const score = ParamPool.TOTAL_SCORE;
        let nick_name = document.querySelector(".nick-name").innerHTML;
        const head_img = document.querySelector(".head-img").innerHTML;
        const openid = document.querySelector(".openid").innerHTML;

        // nick_name = nick_name.substring(1);
        const param = `nick_name=${nick_name}&head_img=${head_img}&score=${score}&openid=${openid}`;
        // console.log(param)

        // alert(param)

        // var xhr = new XMLHttpRequest();
        // var stringData = JSON.stringify({
        //     nick_name,
        //     head_img,
        //     score,
        //     openid
        // });
        //  alert(stringData)
        // //POST请求
        // xhr.open('POST', ParamPool.BASE_URL + "/paihang");
        // xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // xhr.send(stringData)
        // xhr.onreadystatechange = function (res) {
        //     if (xhr.readyState == 4) {
        //         if (xhr.status == 200) {
        //             console.log(JSON.parse(xhr.responseText))
        //             alert(xhr.responseText)
        //         }
        //     }
        // }


        var request = new egret.HttpRequest();
        request.responseType = egret.HttpResponseType.TEXT;
        request.open(ParamPool.BASE_URL + "/paihang", egret.HttpMethod.POST);
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.send(param);
        request.addEventListener(egret.Event.COMPLETE, onGetComplete, this);
        request.addEventListener(egret.IOErrorEvent.IO_ERROR, onPostIOError, this);

        function onGetComplete(evt) {
            console.log(evt.currentTarget.response)
            // alert(evt.currentTarget.response)
            // alert(JSON.stringify(evt.currentTarget.response))
            window.location.href = ParamPool.PRO_PATH + "?" + Math.random();
        }

        function onPostIOError(evt) {
            // alert(evt.currentTarget.response)
            // alert(JSON.stringify(evt.currentTarget.response))
            const tk_png = Main.createBitmapByName("postErr_png");
            miva.addChild(tk_png);
            Main.anchorCenter(tk_png);
            tk_png.x = miva.stage.stageWidth / 2;
            tk_png.y = miva.stage.stageHeight / 2;
            egret.Tween.get(tk_png).wait(500).to({ alpha: 0 }, 200).call(() => {
                window.location.href = ParamPool.PRO_PATH + "?" + Math.random();
            });
            console.log("成绩提交失败!")
        }


    }




}